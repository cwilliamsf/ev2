<%-- 
    Document   : userlist
    Created on : 25-04-2021, 21:28:11
    Author     : camilo williams
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.creacionusuarios.entity.Creacionusuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Creacionusuario> usuarios = (List<Creacionusuario>) request.getAttribute("ListaUsuarios");
    Iterator<Creacionusuario> itusuarios = usuarios.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <form  name="form" action="CreacionController" method="POST">
        
        <table border="1">
          <thead> 
          <th>Rut</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Direccion</th>
          <th>Telefono</th>
          </thead>
          
          <tbody>
              <% while (itusuarios.hasNext()) {
              Creacionusuario cu = itusuarios.next();%>
              <tr>
                <td><%= cu.getRut()%></td>
                <td><%= cu.getNombre()%></td>
                <td><%= cu.getApellido()%></td>
                <td><%= cu.getDireccion()%></td>
                <td><%= cu.getTelefono()%></td>
                <td><input type="radio" name="seleccion" value="<%= cu.getRut()%>"></td>
              </tr>
              <%}%>
              
          </tbody>
        </table>
            <button type="submit" name="bton" value="cambio" class="btn btn-success">Modificar Usuario</button>
            <button type="submit" name="bton" value="eliminar" class="btn btn-success">Eliminar Usuario</button>
        </form>      
    </body>
</html>
