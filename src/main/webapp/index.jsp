<%-- 
    Document   : index
    Created on : 25-04-2021, 20:16:51
    Author     : camilo williams
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Creacion de Usuario</h1>
        <form  name="form" action="CreacionController" method="POST">

            Ingrese RUT:
            <input type="text" id="rut" name="rut">
            Nombre: 
            <input type="text" id="nombre"  name="nombre">
            Apellido:
            <input type="text" id="apellido" name="apellido">
            Direccion:
            <input type="text" id="direccion" name="direccion">
            Telefono:
            <input type="text" id="telefono" name="telefono">
            <button type="submit" name="bton" value="nuevousuario" class="btn btn-success">Crear Usuario</button>
            <button type="submit" name="bton" value="verusuarios" class="btn btn-success">Ver Usuarios</button>

        </form>
    </body>
</html>
