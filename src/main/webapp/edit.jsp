<%-- 
    Document   : index
    Created on : 25-04-2021, 20:16:51
    Author     : camilo williams
--%>

<%@page import="java.util.List"%>
<%@page import="com.mycompany.creacionusuarios.entity.Creacionusuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Creacionusuario modificar = (Creacionusuario) request.getAttribute("Modificar");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Creacion de Usuario</h1>
        <form  name="form" action="CreacionController" method="POST">

            Ingrese RUT:
            <input type="text" id="rut" name="rut" value="<%= modificar.getRut()%>">
            Nombre: 
            <input type="text" id="nombre"  name="nombre" value="<%= modificar.getNombre()%>">
            Apellido:
            <input type="text" id="apellido" name="apellido" value="<%= modificar.getApellido()%>">
            Direccion:
            <input type="text" id="direccion" name="direccion" value="<%= modificar.getDireccion()%>">
            Telefono:
            <input type="text" id="telefono" name="telefono" value="<%= modificar.getTelefono()%>">
            <button type="submit" name="bton" value="editar" class="btn btn-success">Editar</button>

        </form>
    </body>
</html>
