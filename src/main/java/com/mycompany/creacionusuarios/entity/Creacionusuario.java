/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.creacionusuarios.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author camilo williams
 */
@Entity
@Table(name = "creacionusuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Creacionusuario.findAll", query = "SELECT c FROM Creacionusuario c"),
    @NamedQuery(name = "Creacionusuario.findByRut", query = "SELECT c FROM Creacionusuario c WHERE c.rut = :rut"),
    @NamedQuery(name = "Creacionusuario.findByNombre", query = "SELECT c FROM Creacionusuario c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Creacionusuario.findByApellido", query = "SELECT c FROM Creacionusuario c WHERE c.apellido = :apellido"),
    @NamedQuery(name = "Creacionusuario.findByDireccion", query = "SELECT c FROM Creacionusuario c WHERE c.direccion = :direccion"),
    @NamedQuery(name = "Creacionusuario.findByTelefono", query = "SELECT c FROM Creacionusuario c WHERE c.telefono = :telefono")})
public class Creacionusuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;

    public Creacionusuario() {
    }

    public Creacionusuario(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Creacionusuario)) {
            return false;
        }
        Creacionusuario other = (Creacionusuario) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.creacionusuarios.entity.Creacionusuario[ rut=" + rut + " ]";
    }
    
}
