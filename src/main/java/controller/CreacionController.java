/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.creacionusuarios.dao.CreacionusuarioJpaController;
import com.mycompany.creacionusuarios.dao.exceptions.NonexistentEntityException;
import com.mycompany.creacionusuarios.entity.Creacionusuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author camilo williams
 */
@WebServlet(name = "CreacionController", urlPatterns = {"/CreacionController"})
public class CreacionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreacionController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreacionController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");
        String lista = request.getParameter("bton");
        if (lista.equals("nuevousuario")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String direccion = request.getParameter("direccion");
                String telefono = request.getParameter("telefono");

                Creacionusuario creacionusuario = new Creacionusuario();
                creacionusuario.setRut(rut);
                creacionusuario.setNombre(nombre);
                creacionusuario.setApellido(apellido);
                creacionusuario.setDireccion(direccion);
                creacionusuario.setTelefono(telefono);

                CreacionusuarioJpaController dao = new CreacionusuarioJpaController();

                dao.create(creacionusuario);
                

            } catch (Exception ex) {
                Logger.getLogger(CreacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        if (lista.equals("verusuarios")) {
            CreacionusuarioJpaController dao = new CreacionusuarioJpaController();
            List<Creacionusuario> listauser = dao.findCreacionusuarioEntities();
            request.setAttribute("ListaUsuarios", listauser);
            request.getRequestDispatcher("userlist.jsp").forward(request, response);

        }
        if (lista.equals("cambio")) {
            String rut = request.getParameter("seleccion");
            CreacionusuarioJpaController dao = new CreacionusuarioJpaController();
            Creacionusuario modificacion = dao.findCreacionusuario(rut);
            request.setAttribute("Modificar", modificacion);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            
        
        }
        if (lista.equals("eliminar")) {
            try {
                String rut = request.getParameter("seleccion");
                CreacionusuarioJpaController dao = new CreacionusuarioJpaController();
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(CreacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
